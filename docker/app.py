#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""App module."""
import datetime
import logging

import connexion
from connexion.exceptions import ProblemException

import extraction


# function for CI_view

def diagrams_data(begin_date, end_date, case_name, limit_nb, pod_name, option):
    """Transform raw result data into time series."""
    # with date in the form : "2019-06-07 20:17:46"
    begin_day = begin_date[:10]  # string extraction
    begin_time = begin_date[-8:]
    end_day = end_date[:10]
    end_time = end_date[-8:]
    if ((extraction.days_diff(begin_day[8:] + "/" + begin_day[5:7] + "/" + begin_day[:4],
                              end_day[8:] + "/" + end_day[5:7] + "/" + end_day[:4]) < 0)
            or (extraction.days_diff(begin_day[8:] + "/" + begin_day[5:7] + "/" + begin_day[:4],
                                     end_day[8:] + "/" + end_day[5:7] + "/" + end_day[:4]) == 0
                and datetime.time(int(begin_time[:2]), int(begin_time[3:5]),
                                  int(begin_time[6:8])) > datetime.time(int(end_time[:2]),
                                                                        int(end_time[3:5]),
                                                                        int(end_time[6:8])))):
        raise ProblemException(400, "Date error", "end_date before begin_date",
                               "Malformed request syntax error")  # test for date

    if case_name not in ["onap-k8s", "onap-helm", "core", "small", "medium", "full", "healthdist",
                         "basic_vm", "basic_onboard", "basic_clamp", "basic_vm_macro",
                          "postinstall","basic_cnf","basic_network",
                          "pnf-registrate","5gbulkpm","cmpv2","hv-ves","dcaemod",
                          "ves-collector","root_pods","jdpw_ports",
                          "nonssl_endpoints","unlimitted_pods"]:
        raise ProblemException(400, "case_name error", "Please choose in this list: "
                               "onap-k8s, onap-helm, core, small,"
                               " medium, full, healthdist, basic_vm, "
                               "freeradius_nbi, clearwater_ims, vfw",
                               "postinstall","basic_cnf","basic_network",
                               "pnf-registrate","5gbulkpm","cmpv2","hv-ves",
                               "ves-collector","root_pods","jdpw_ports",
                               "nonssl_endpoints","unlimitted_pods"
                               "Malformed request syntax error")

    if option == "diagram_pod":
        results = extraction.time_serie_from_test(extraction.demande(
            "http://testresults.opnfv.org/onap/api/v1/results",
            begin_date, end_date, limit_nb, pod_name, case_name))
        if len(results) == 2:  # if we don't have enough data
            return {'message': str(results[:2]), 'begin_date': begin_date,
                    'end_date': end_date}, 200, {'Access-Control-Allow-Origin': "*"}
        return {'message': str(results[:2]), 'begin_date': str(results[2]),
                'end_date': str(results[3])}, 200, {'Access-Control-Allow-Origin': "*"}

    if option == "average_pod":
        results = extraction.average_test(extraction.demande(
            "http://testresults.opnfv.org/onap/api/v1/results",
            begin_date, end_date, limit_nb, pod_name, case_name))
        if len(results) == 2:  # if we don't have enough data
            return {'message': str(results[:2]), 'begin_date': begin_date,
                    'end_date': end_date}, 200, {'Access-Control-Allow-Origin': "*"}
        return {'message': str(results[:2]), 'begin_date': str(results[2]),
                'end_date': str(results[3])}, 200, {'Access-Control-Allow-Origin': "*"}

    if option == "average_success":
        results = extraction.average_success(extraction.demande(
            "http://testresults.opnfv.org/onap/api/v1/results",
            begin_date, end_date, limit_nb, pod_name, case_name))
        if len(results) == 2:  # if we don't have enough data
            return {'message': str(results[:2]), 'begin_date': begin_date,
                    'end_date': end_date}, 200, {'Access-Control-Allow-Origin': "*"}
        return {'message': str(results[:2]), 'begin_date': str(results[2]),
                'end_date': str(results[3])}, 200, {'Access-Control-Allow-Origin': "*"}

    raise ProblemException(400, "Option unknown", "Please choose in this list: "
                           "diagram_pod, average_pod, average_success",
                           "Malformed request syntax error")


def ping_return():
    """Ping function."""
    return {'Ping': 'Pong'}, 200, {'Access-Control-Allow-Origin': "*"}


def accuracy(begin_date, end_date, limit_nb, pod_name):
    """Give more information about the chained tests."""
    return {'message': extraction.test_chaine(extraction.demande(
        "http://testresults.opnfv.org/onap/api/v1/results", begin_date, end_date, limit_nb+10,
        # +10 to be sure to end a chained test series
        pod_name))}, 200, {'Access-Control-Allow-Origin': "*"}


def api_launch():
    """Launch the app."""
    if __name__ == '__main__':
        logging.basicConfig(level=logging.INFO)
        app = connexion.FlaskApp(__name__)
        app.add_api('swagger.yaml')

        app.run(
            port=8080,
            threaded=False  # in-memory database isn't shared across threads
        )


api_launch()
