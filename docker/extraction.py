# -*- coding: utf-8 -*-
"""Function library to extract data."""
from datetime import date

import sys
import requests


def demande(source, begin_date, end_date, limit_nb, pod_name, case_name = ""):
    """Enter parameters to get clean and simplified data."""
    compteur = limit_nb
    data = []

    print("", file=sys.stderr)  # un espace pour une meilleure lecture des logs

    # process
    # adresse de la premiere page
    if case_name == "":  # si aucun case_name n'est indique
        adresse = (source + "?from=" + begin_date + "&to=" + end_date + "&pod_name=" + pod_name)
    else:
        adresse = (source + "?from=" + begin_date + "&to=" + end_date + "&case_name=" + case_name
               + "&pod_name=" + pod_name)
    print("request test: " + adresse, file=sys.stderr)
    test_request = requests.get(adresse)
    if test_request.status_code < 300:  # cas dans lequel la requete se passe bien
        temp = {}
        get_arg_from_json(test_request.text, "total_pages", temp)
        # correspond au nombre de page a parcourir
        max_page = int(temp["total_pages"])

        if limit_nb > (max_page - 1) * 20:  # on regarde s'il y a assez de donnees dans les pages
            compteur = max_page * 20  # sinon on prend autant de donnee que possible

        nb_page = 1
        while compteur > 20:
            print("request to: " + adresse + "&page=" + str(nb_page), file=sys.stderr)
            for i in join_server(adresse + "&page=" + str(nb_page)):
                data.append(i)
            compteur -= 20
            nb_page += 1
        # on a obtenu toutes les donnees presentent dans les pages pleines
        # on passe a la page qui n'est pas pliene (si besoin)
        # on doit recuperer le maximum de donnees, sans depasser
        print("request to: " + adresse + "&page=" + str(nb_page), file=sys.stderr)
        for i in join_server(adresse + "&page=" + str(nb_page)):
            if len(data) < limit_nb:
                data.append(i)
    else:
        print("Erreur lors de la communication initiale.", file=sys.stderr)
    return data


def join_server(adresse):
    """Test the communication with the server."""
    test_request = requests.get(adresse)
    if test_request.status_code < 300:  # cas dans lequel la requete se passe bien
        return decoupe_json(test_request)
    print("Erreur lors de la communication.", file=sys.stderr)
    return None


def decoupe_json(test_request):
    """Split the json data by separating each test."""
    json = test_request.text
    data = []

    # on recherche toutes les iterations de "project_name", correspondand au debut d'un nouveau test
    balise = []
    cursor = 0
    while cursor + 1 < (len(json) - len("project_name")):
        if json[cursor: cursor + len("project_name")] == "project_name":
            balise.append(cursor)
        cursor += 1
    balise.append(len(json))  # on rajoute la fin du document comme balise

    for i in range(0, json.count("project_name")):  # on decoupe le json pour avoir tous les essais
        part = json[balise[i]:balise[i + 1]]
        data.append(get_info_from_json(part))
    return data


def get_info_from_json(jsonfile):
    """Extract the different information present in the json file."""
    diction = {}
    get_arg_from_json(jsonfile, "start_date", diction)  # recupere les infos correspondant a "..."
    get_arg_from_json(jsonfile, "stop_date", diction)  # et les met dans un dictionnaire
    get_arg_from_json(jsonfile, "case_name", diction)
    get_arg_from_json(jsonfile, "criteria", diction)
    get_arg_from_json(jsonfile, "build_tag", diction)

    # on realise la distinction de cas en fonction des case_name
    # if diction["case_name"] == "onap-k8s" or diction["case_name"] == "onap-helm":
    #     get_pod_from_json(jsonfile, diction)
    # else:
    if (diction["case_name"] == "core" or diction["case_name"] == "small"
            or diction["case_name"] == "medium" or diction["case_name"] == "full"
            or diction["case_name"] == "healthdist"
            or diction["case_name"] == "postinstall"
            or diction["case_name"] == "hv-ves"
            or diction["case_name"] == "ves-collector"
            or diction["case_name"] == "dcaemod"
            or diction["case_name"] == "cmpv2"
            or diction["case_name"] == "5gbulkpm"
            or diction["case_name"] == "pnf-registrate"):
        get_bot_from_json(jsonfile, diction)
    else:  # cas de 'basic_vm', 'freeradius_nbi', 'clearwater_ims', ou 'vfw' (resultat binaire)
        if diction["criteria"] == "PASS":
            diction["result"] = "100.0"
        else:
            diction["result"] = "0.0"
    return diction


def get_arg_from_json(jsonfile, arg, diction):
    """Search for strings of character in the json, and add them to the dictionary."""
    for i in range(0, len(jsonfile) - len(arg)):
        if jsonfile[i:i + len(arg)] == arg:
            # i est a la place du mot recherche, on va prendre les caracteres qui sont juste apres
            espace = i + len(arg)
            while jsonfile[espace] != " " and (espace + 1) < len(jsonfile):
                espace += 1  # correspond a l'indice de l'espace qui precede la donnee
            ending = espace
            while jsonfile[ending] != "}" and jsonfile[ending] != "," and ending+1 < len(jsonfile):
                ending += 1  # on recherche la fin de la donnee, qui correspond au caractere , ou }
            donnee = jsonfile[espace + 1: ending]

            # resultat obtenu, on va maintenant enlever les guillemets qui pourrait l'encadrer
            if donnee[0: 1] == '"':
                donnee = donnee[1:-1]
            diction[arg] = donnee  # on rajoute le resultat au dictionnaire
            return 0  # on renvoi le fait qu'il n'y a pas eu d'erreur
    return 1  # signe qu'une erreur a eu lieu : la boucle n'a pas fonctionne correctement


def get_pod_from_json(jsonfile, diction):
    """Compile the results of the pods."""
    if diction["case_name"] == "onap-k8s":
        pods = {}
        get_arg_from_json(jsonfile, "Nb Failed Pods", pods)
        get_arg_from_json(jsonfile, "Nb Pods", pods)
        try:  # on verifie s'il y a des donnees dans le dictionnaire
            nb_max = int(pods["Nb Pods"])
            nb_fault = int(pods["Nb Failed Pods"])
        except KeyError:
            nb_max = 0
            nb_fault = 0
    else:  # cas dans lequel on a un test de type 'onap-helm'
        pods = {}
        get_arg_from_json(jsonfile, "Nb Failed Helm Charts", pods)
        get_arg_from_json(jsonfile, "Nb Helm Charts", pods)
        try:
            nb_max = int(pods["Nb Helm Charts"])
            nb_fault = int(pods["Nb Failed Helm Charts"])
        except KeyError:
            nb_max = 0
            nb_fault = 0

    if nb_max > 0:
        resultat = (nb_max - nb_fault) * 100 / nb_max
    else:
        resultat = "0.0"
    diction["result"] = str(resultat)


def get_bot_from_json(jsonfile, diction):
    """Compile the results of the bots."""
    # on a plusieur robot qui ont fait des tests, et mis les resultats dans status
    # on va compter le nombre de resultat positif, et le nombre de negatif.
    good = 0
    bad = 0
    cursor = 0
    liste_pod_fail=[]
    while cursor + 1 < (len(jsonfile) - len("status")):
        if jsonfile[cursor: cursor + len("status")] == "status":  # cursor est au debut du resultat
            # les resultats sont tous de la meme taille (de taille 4), et toujours au meme endroit
            res = jsonfile[cursor + len('status": "'):cursor + len('status": "') + 4]
            if res == "PASS":
                good += 1
            else:
                bad += 1
                if diction["case_name"] in ("core", "full"):
                      # on veut le nom du pod qui est faux
                    get_arg_from_json(jsonfile[cursor:], "name", diction)
                    liste_pod_fail.append(diction["name"])
        cursor += 1

    if (good + bad) > 0:
        resultat = good * 100 / (good + bad)
    else:
        resultat = "0.0"
    diction["result"] = str(resultat)  # on rajoute le resultat sous forme de pourcentage

    if "name" in diction:  # cas dans lequel il y a eu au moins un pod FAIL, et on a son nom
        del diction["name"]  # on va changer le nom de la reference dans le dictionnaire
        diction["pod_fail"] = liste_pod_fail


def day_parse(my_date):
    """Transform date into comparable values."""
    day, month, year = map(int, my_date.split('/'))
    return date(year, month, day)


def days_diff(begin_date, end_date):
    """Calculate the day difference between two dates."""
    # date of type 15/05/1972
    return (day_parse(end_date) - day_parse(begin_date)).days


# on va maintenant exploiter les donnees
def time_serie_from_test(data):  # avec data sous la forme d'une liste de dictionnaire
    """Turn the data extracted into time series (use the result of each pod)."""
    # initialisation
    x_data = []
    y_data = []

    # on regarde dans un premier temps s'il y a des donnees, et on renvoi rien sinon
    if len(data)==0:
        return x_data, y_data

    # test de data:
    if not data:
        return x_data, y_data
        # print(pas de data extraite, file=sys.stderr)

    # les donnees les plus recente sont au debut de data, or on veut les afficher a la fin
    data.reverse()

    # y_data est facile a remplir : il suffit de mettre tous les resultats, en repassant en float
    for i in data:
        y_data.append(round(float(i["result"]), 3))

    # pour x_data, on va devoir regarder en fonction des jours
    # on va d'abord prendre la taille maximale de l'intervalle de jour:
    begin_date = data[0]["start_date"]
    end_date = data[-1]["start_date"]
    longueur_diff = abs(days_diff(begin_date[8:10] + "/" +
                                  begin_date[5:7] + "/" + begin_date[0:4], end_date[8:10] + "/" +
                                  end_date[5:7] + "/" + end_date[0:4]))  # on formate les dates

    # on regarde maintenant le nombre de donnees par jour. (on fait une liste de 0)
    result_per_day = [0] * (longueur_diff + 1)
    for i in data:
        indice = abs(days_diff(begin_date[8:10] + "/" +
                               begin_date[5:7] + "/" + begin_date[0:4], i["start_date"][8:10] +
                               "/" + i["start_date"][5:7] + "/" + i["start_date"][0:4]))
        result_per_day[indice] += 1

    # on sait alors combien il y a de jours, et en combien d'intervalle on doit decouper chaque jour
    for i in range(0, longueur_diff + 1):  # un resultat pour chaque jour, le dernier compris
        for j in range(0, result_per_day[i]):
            x_data.append(i + j / result_per_day[i])

    return x_data, y_data, begin_date, end_date


def average_success(data):  # avec data sous la forme d'une liste de dictionnaire
    """Turn the data extracted into time series made from average result each days."""
    # on va mettre en y_data les valeurs moyennes de reussite de chaque jour (criteria)
    # initialisation
    x_data = []
    y_data = []

    # on regarde dans un premier temps s'il y a des donnees, et on renvoi rien sinon
    if len(data)==0:
        return x_data, y_data

    # les donnees les plus recente sont au debut de data, or on veut les afficher à la fin
    data.reverse()

    # pour x_data, on va devoir regarder en fonction des jours
    # on va d'abord prendre la taille maximale de l'intervalle de jour:
    begin_date = data[0]["start_date"]
    end_date = data[-1]["start_date"]
    longueur_diff = abs(days_diff(begin_date[8:10] + "/" + begin_date[5:7] + "/" +
                                  begin_date[0:4], end_date[8:10] + "/" + end_date[5:7] + "/" +
                                  end_date[0:4]))  # on formate les dates

    # on regarde combien il y a de donnees par jour. (on met sous forme d'une liste de 0)
    result_per_day = [0] * (longueur_diff+1)  # stocke le nombre de test fait
    somme_per_day = [0] * (longueur_diff+1)  # stocke le nombre de reussite
    for i in data:
        indice = abs(days_diff(begin_date[8:10] + "/" + begin_date[5:7] + "/" +
                               begin_date[0:4], i["start_date"][8:10] + "/" +
                               i["start_date"][5:7] + "/" + i["start_date"][0:4]))
        result_per_day[indice] += 1
        if i["criteria"] == "PASS":
            somme_per_day[indice] += 1

    # on sait maintenant combien il y a de jours
    for i in range(0, longueur_diff + 1):  # un resultat pour chaque jour, le dernier compris
        x_data.append(i)

    # partie pour y_data, dependant de la moyenne pour chaque jour
    erase_list = []  # on va utiliser cette liste pour reduire x_data (pour les jours sans test)
    for i in range(0, longueur_diff + 1):
        if result_per_day[i] > 0:  # si on a au moins 1 résultat dans ce jour
            y_data.append(round(somme_per_day[i] / result_per_day[i] * 100, 3))  # fait la moyenne du jour
        else:  #cas dans lequel aucun test n'a ete lance (donc pas de resulat)
            erase_list.append(i)# on ajoute un element a la liste

    # partie destructive ou l'on remet x_data coherent par rapport a y_data
    erase_list.sort()
    erase_list.reverse()  # on va faire les elements au fond de la liste avant
    for i in erase_list:
        del x_data[i]  # on supprime les coordonnees en x qui n'ont pas d'equivalent en y

    return x_data, y_data, begin_date, end_date


def average_test(data):  # avec data sous la forme d'une liste de dictionnaire
    """Turn the data extracted into time series (make an average of each test per days)."""
    # on va mettre en y_data les valeurs moyennes de chaque pods de chaque jour (result)
    # initialisation
    x_data = []
    y_data = []

    # on regarde dans un premier temps s'il y a des donnees, et on renvoi rien sinon
    if len(data)==0:
        return x_data, y_data

    # les donnees les plus recente sont au debut de data, or on veut les afficher à la fin
    data.reverse()

    # pour x_data, on va devoir regarder en fonction des jours
    # on va d'abord prendre la taille maximale de l'intervalle de jour:
    begin_date = data[0]["start_date"]
    end_date = data[-1]["start_date"]
    longueur_diff = abs(days_diff(begin_date[8:10] + "/" + begin_date[5:7] + "/" +
                                  begin_date[0:4], end_date[8:10] + "/" + end_date[5:7] + "/" +
                                  end_date[0:4]))  # on formate les dates

    # on regarde combien il y a de donnees par jour. (on met sous forme d'une liste de 0)
    result_per_day = [0] * (longueur_diff+1)  # stocke le nombre de test fait
    somme_per_day = [0] * (longueur_diff+1)  # stocke le nombre de reussite
    for i in data:
        indice = abs(days_diff(begin_date[8:10] + "/" + begin_date[5:7] + "/" +
                               begin_date[0:4], i["start_date"][8:10] + "/" +
                               i["start_date"][5:7] + "/" + i["start_date"][0:4]))
        result_per_day[indice] += 1
        somme_per_day[indice] += float(i["result"])

    # on sait maintenant combien il y a de jours
    for i in range(0, longueur_diff + 1):  # un resultat pour chaque jour, le dernier compris
        x_data.append(i)

    # partie pour y_data, dependant de la moyenne pour chaque jour
    erase_list = []  # on va utiliser cette liste pour reduire x_data (pour les jours sans test)
    for i in range(0, longueur_diff + 1):
        if result_per_day[i] > 0:  # si on a au moins 1 résultat dans ce jour
            y_data.append(round(somme_per_day[i] / result_per_day[i], 3))  # fait la moyenne du jour
        else:  #cas dans lequel aucun test n'a ete lance (donc pas de resulat)
            erase_list.append(i)# on ajoute un element a la liste

    # partie destructive ou l'on remet x_data coherent par rapport a y_data
    erase_list.sort()
    erase_list.reverse()  # on va faire les elements au fond de la liste avant
    for i in erase_list:
        del x_data[i]  # on supprime les coordonnees en x qui n'ont pas d'equivalent en y

    return x_data, y_data, begin_date, end_date


def test_chaine(data):
    """Extract information about each chained test in the data."""
    if len(data) == 0:
        return []
    index = 0
    table = [[data[0]["build_tag"], data[0]["start_date"], 0, " None ", [], " None ", [],
              "Not run"]]
    # liste du type: [[tag, date, nb, core%, pod core, full%, pod full, healthdist], ...]
    while index<len(data):
        temp = [table[j][0] for j in range(len(table))]

        if not data[index]["build_tag"] in temp:  # on commence une nouvelle ligne si elle n'existe
            table.append([data[index]["build_tag"], data[index]["start_date"], 0, " None ", [],
                          " None ", [], "Not run"])
        # on va separer les cas, pour recuperer ceux qui nous interresse
        if data[index]["case_name"] == "core":
            table[-1][3] = round(float(data[index]["result"]), 2)
            if "pod_fail" in data[index]:
                table[-1][4] = data[index]["pod_fail"]
                table[-1][2] += len(data[index]["pod_fail"])
        else:
            if data[index]["case_name"] == "full":
                table[-1][5] = round(float(data[index]["result"]), 2)
                if "pod_fail" in data[index]:
                    table[-1][6] = data[index]["pod_fail"]
                    table[-1][2] += len(data[index]["pod_fail"])
            else:
                if data[index]["case_name"] == "healthdist":
                    if float(data[index]["result"])==100.0:
                        table[-1][7] = "OK"
                    else:
                        table[-1][7] = "KO"
        index += 1
    return table[:-1]  # on enleve le dernier element, qui pourrait etre incomplet

# # exemple d'utilisation
# infos = demande("http://testresults.opnfv.org/onap/api/v1/results", "2019-05-13 14:45:57",
#  "2019-06-12 19:45:27", 150, "full")
# data_to_figure(infos)
