FROM python:3.6-alpine

# import for python files
RUN pip3 install connexion
RUN pip3 install connexion[swagger-ui]

COPY docker/app.py app.py
COPY docker/extraction.py extraction.py
COPY docker/swagger.yaml swagger.yaml

ENV PORT 8080
EXPOSE 8080
CMD ["python3.6", "app.py"]
